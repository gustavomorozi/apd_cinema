import unittest

from logica import sessao
from logica import filme
from logica import sala

class TestSessao(unittest.TestCase):

        def setUp(self):
                sessao.remover_todas_sessoes()

        def test_sem_sessoes(self):
                sessoes= sessao.lista_sessao()
                self.assertEqual(0,len(sessoes))

        def test_cadastrar_sessao(self):
                sessao.cadastrar_sessao(1,1,"15:15")
                sessao.cadastrar_sessao(2,2,"21:15")
                self.assertEqual(2,len(sessao.sessoes))

        def test_buscar_sessao(self):
                sessao.cadastrar_sessao(1,1,"15:15")
                self.assertIsNone(sessao.buscar_sessao(0))


        def test_remover_sessao(self):
                sessao.cadastrar_sessao(1,1,"15:15")
                sessao.cadastrar_sessao(2,2,"21:15")

                sessao.remover_sessao(0)

                s= sessao.buscar_sessao(0)

                self.assertIsNone(s)


        def test_iniciar_sessoes(self):
                sessao.iniciar_sessoes()
                sessoes= sessao.lista_sessao()
                self.assertEqual(2,len(sessoes))
                

                

        


if __name__ == '__main__':
    unittest.main(exit=False)
