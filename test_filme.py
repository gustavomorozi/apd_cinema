import unittest
from logica import filme

class Testefilme(unittest.TestCase):

        def setUp(self):
                filme.cadastrar_filme('As Branquelas','2:30:00','_14','Keenen Ivory Wayans','FOX','Ativo','Comédia')
                filme.cadastrar_filme('Sexta feira 13','1:30:00','+18','Paul McDonalds','Century','Inativo','Terror')
                filme.remover_todos_filmes()


        def test_cadastrar_filme(self):
                filme.cadastrar_filme('As Branquelas','2:30:00','_14','Keenen Ivory Wayans','FOX','Ativo','Comédia')
                filme.cadastrar_filme('Sexta feira 13','1:30:00','+18','Paul McDonalds','Century','Inativo','Terror')
                self.assertEqual(2,len(filme.filmes))

        def test_buscar_filme(self):
                filme.cadastrar_filme('As Branquelas','2:30:00','_14','Keenen Ivory Wayans','FOX','Ativo','Comédia')
                self.assertIsNone(filme.buscar_filme(0))

        def test_remover_filme(self):
                filme.cadastrar_filme('As Branquelas','2:30:00','_14','Keenen Ivory Wayans','FOX','Ativo','Comédia')
                filme.cadastrar_filme('Sexta feira 13','1:30:00','+18','Paul McDonalds','Century','Inativo','Terror')
                filme.remover_filme(0)
                a= filme.buscar_filme(0)
                self.assertIsNone(a)

        def test_remover_todos_filmes(self):
                filme.cadastrar_filme('As Branquelas','2:30:00','_14','Keenen Ivory Wayans','FOX','Ativo','Comédia')
                filme.cadastrar_filme('Sexta feira 13','1:30:00','+18','Paul McDonalds','Century','Inativo','Terror')
                filme.remover_todos_filmes()
                a = filme.buscar_filmes()
                self.assertEqual([], a)

        def test_iniciar_filmes(self):
               filme.iniciar_filmes()
               filmes = filme.buscar_filmes()
               self.assertEqual(2,len(filmes))

        def test_sem_filme(self):
                filmes=filme.buscar_filmes()
                self.assertEqual(0,len(filmes))


if __name__=='__main__':
    unittest.main()
