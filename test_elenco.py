import unittest

from logica import elenco
from logica import filme


class TesteElenco(unittest.TestCase):

        def setUp(self):
                elenco.cadastrar_elenco(1,1,"Coadjuvante")
                elenco.cadastrar_elenco(2,2,"Principal")
                elenco.remover_todos_elencos()
                

        def test_cadastrar_elenco(self):
                elenco.cadastrar_elenco(1,1,"Coadjuvante")
                elenco.cadastrar_elenco(2,2,"Principal")
                self.assertEqual(2,len(elenco.elencos))
                
        def test_buscar_elenco_codigo(self):
                elenco.cadastrar_elenco(1, 1, "Coadjuvante")
                self.assertIsNone(elenco.buscar_elenco_codigo(0))

        def test_remover_elenco(self):
                elenco.cadastrar_elenco(1,1,"Coadjuvante")
                elenco.cadastrar_elenco(2,2,"Principal")

                elenco.remover_elencos(0)

                e=elenco.buscar_elenco_codigo(0)

                self.assertIsNone(e)


        def test_iniciar_elencos(self):
                elenco.iniciar_elencos()
                elencos = elenco.lista_elencos()
                self.assertEqual(2,len(elencos))

        def test_sem_elenco(self):
                elencos=elenco.lista_elencos()
                self.assertEqual(0,len(elencos))
                


if __name__=='__main__':
    unittest.main()

        
