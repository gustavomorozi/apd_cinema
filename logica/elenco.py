from logica import filme
from logica import ator

elencos = []
codigo_geral = 0 

def gerar_codigo():
    global codigo_geral
    codigo_geral += 1
    return codigo_geral


def cadastrar_elenco(cod_filme,cod_ator,tipo):
    cod_elenco=gerar_codigo()
    f = cod_filme
    a = cod_ator
    cod_elenco = gerar_codigo()
    elenco = [cod_elenco,f, a, tipo]
    elencos.append(elenco)

def buscar_elenco_codigo(cod_elenco):
    for e in elencos:
        if e[0] == cod_elenco:
            return e
    return None

def lista_elencos():
    return elencos

def buscar_elenco_filme(cod_filme):
    aux=[]
    for e in elencos:
        if e[1] == cod_filme:
            aux.append(e)
    return aux

 

def remover_elencos(cod_elenco):
    for e in elencos:
        if(e[0]==cod_elenco):
            elencos.remove(e)
            return True
    return False
    
    

def remover_todos_elencos():
    if len(elencos)!=0:
        elencos.clear()
        return True

def iniciar_elencos():
    cadastrar_elenco(1,1,"Coadjuvante")
    cadastrar_elenco(2,2,"Principal")



