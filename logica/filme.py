filmes=[]
codigo_geral = 0

def _gerar_codigo():
    global codigo_geral
    codigo_geral += 1
    return codigo_geral

def cadastrar_filme(titulo,duracao,classificacao,diretor,distribuidora,status,genero):
    cod_filme=_gerar_codigo()
    filme=[cod_filme,titulo,duracao,classificacao,diretor,distribuidora,status,genero]
    filmes.append(filme)

def buscar_filme(cod_filme):
    for f in filmes:
        if f[0]==cod_filme:
            return f
    return None

def remover_filme(cod_filme):
    for f in filmes:
        if f[0]==cod_filme:
            filmes.remove(f)
            return True
    return False

def buscar_filmes():
    return filmes

def remover_todos_filmes():
    if len(filmes)!=0:
        filmes.clear()
        return True

def iniciar_filmes():
    cadastrar_filme('As Branquelas','2:30:00','_14','Keenen Ivory Wayans','FOX','Ativo','Comédia')
    cadastrar_filme('Sexta feira 13','1:30:00','+18','Paul McDonalds','Century','Inativo','Terror')




