from logica import filme
from logica import sala

sessoes = []
codigo_geral = 0

def gerar_codigo():
        global codigo_geral
        codigo_geral += 1
        return codigo_geral

def cadastrar_sessao(cod_filme,cod_sala,horario):
        f = filme.buscar_filme(cod_filme)
        s = sala.buscar_sala(cod_sala)
        cod_sessao = gerar_codigo()
        sessao= [cod_sessao,f,s,horario]
        sessoes.append(sessao)

def recuperar_sessao(cod_sessao):
    for s in sessoes:
        if s[0] == cod_sessao:
            return s
    return None

def verificar_lotacao(cod_sessao):
    for s in sessoes:
        if s[0] == cod_sessao:
            sessao= sala.buscar_sala(s[0])
            return sessao[1]
    return None

def buscar_sessao(cod_sessao):
    for s in sessoes:
        if (s[0] == cod_sessao):
            return s
    return None


def remover_sessao(cod_sessao):
        for s in sessoes:
                if s[0]== cod_sessao:
                        sessoes.remove(s)
                        return True
        return False

def lista_sessao():
   return sessoes

def remover_todas_sessoes():
        if len(sessoes)!=0:
                sessoes.clear()
                return True

def iniciar_sessoes():
        cadastrar_sessao(1,1, '15:30')
        cadastrar_sessao(2,2,'21:50')
