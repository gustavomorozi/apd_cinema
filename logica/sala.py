salas=[]
codigo_geral = 0

def _gerar_codigo():
    global codigo_geral
    codigo_geral += 1
    return codigo_geral

def cadastrar_sala(lotacao):
    cod_sala=_gerar_codigo()
    sala=[cod_sala,lotacao,None]
    salas.append(sala)

def buscar_sala(cod_sala):
    for s in salas:
        if s[0]==cod_sala:
            return s
    return None

def remover_sala(cod_sala):
    for s in salas:
        if s[0]==cod_sala:
            salas.remove(s)
            return True
    return False

def buscar_salas():
    return salas

def remover_todos_salas():
    if len(salas)!=0:
        salas.clear()
        return True

def definir_status_ocupada(cod_sala):
    for s in salas:
        if s[0]==cod_sala:
            s[2]='Ocupada'
        return True
    return False

def definir_status_livre(cod_sala):
    for s in salas:
        if s[0]==cod_sala:
            s[2]='Livre'
        return True
    return False

def iniciar_salas():
    cadastrar_sala(30)
    cadastrar_sala(40)
