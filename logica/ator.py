atores=[]
codigo_geral = 0

def _gerar_codigo():
    global codigo_geral
    codigo_geral += 1
    return codigo_geral

def cadastrar_ator(nome,nacionalidade,idade):
    cod_ator=_gerar_codigo()
    ator=[cod_ator,nome,nacionalidade,idade]
    atores.append(ator)

def buscar_ator(cod_ator):
    for a in atores:
        if a[0]==cod_ator:
            return a
    return None

def remover_ator(cod_ator):
    for a in atores:
        if a[0]==cod_ator:
            atores.remove(a)
            return True
    return False

def buscar_atores():
    return atores

def remover_todos_atores():
    if len(atores)!=0:
        atores.clear()
        return True

def iniciar_atores():
    cadastrar_ator('Bradd Pitt', 'Americano',21)
    cadastrar_ator('Adriane Galisteu', 'Brasileira',29)




