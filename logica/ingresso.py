from logica import sessao

ingressos = []
codigo_geral = 0
def _gerar_codigo():
    global codigo_geral
    codigo_geral += 1
    return codigo_geral


def venda_ingresso(tipo, cod_sessao):
    cod_ingresso = _gerar_codigo()
    ingresso = [cod_ingresso, tipo, cod_sessao]
    ingressos.append(ingresso)

def listar_ingressos_vendidos(cod_sessao):
    aux = []
    for i in ingressos:
        if i[2] == cod_sessao:
            aux.append(i)
    if not aux == []:
        return (aux)
    return None


def listar_ingressos():
    return ingressos


def buscar_ingresso(cod_ingresso):
    for i in ingressos:
        if i[0]==cod_ingresso:
            return i
    return None

def remover_ingresso(cod_ingresso):
    for i in ingressos:
        if i[0]== cod_ingresso:
            ingressos.remove(i)
            return True
    return False

def remover_todos_ingressos():
    if len(ingressos)!=0:
        ingressos.clear()
        return True

def iniciar_ingressos():
    venda_ingresso('MEIA', 1)
    venda_ingresso('INTEIRA', 2)
