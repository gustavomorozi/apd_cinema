from logica import paciente
from logica import medico

consultas = []
codigo_geral = 0

def _gerar_codigo():
    global codigo_geral
    codigo_geral += 1
    return codigo_geral
    

def marcar_consulta(data, cpf_paciente, crm_medico):
    codigo = _gerar_codigo()
    
    p = paciente.buscar_paciente(cpf_paciente)
    m = medico.buscar_medico(crm_medico)
   
    consulta = [codigo, data, p, m]
    consultas.append(consulta)
    return codigo
    
def listar_consultas():
    return consultas    

def buscar_consulta(cod_consulta):
    for c in consultas:
        if (c[0] == cod_consulta):
            return c
    return None

def desmarcar_consulta(codigo_consulta):
    for c in consultas:
        if (c[0] == codigo_consulta):
            consultas.remove(c)
            return True
    return False

def remover_todas_consultas():
    global consultas
    global codigo_geral
    
    consultas = []
    codigo_geral = 0
    
def iniciar_consultas():
    marcar_consulta("12/02/2017", 22222222222, 2222)
    marcar_consulta("12/08/2017", 11111111111, 1111)
    marcar_consulta("13/08/2017", 22222222222, 1111)
