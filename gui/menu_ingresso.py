from logica import ingresso
from logica import sessao

def imprimir_ingresso(ingresso):
    codigo = (ingresso[0])
    t = (ingresso[1])
    s = (ingresso[2])
    print ('Código do Ingresso:', codigo)
    print ('Tipo de Ingresso:', t)
    print ('Código da Sessão:', s)

    print ('\n')


def menu_venda():
    print ('\n Venda de Ingressos \n')
    tipo = str(input('Digite o tipo de ingresso- "MEIA" ou "INTEIRA": '))
    while ((tipo!="MEIA") and (tipo!="INTEIRA")):
        tipo = str(input('Digite o tipo de ingresso- "MEIA" ou "INTEIRA": '))
    sessao = int(input('Digite o código da sessão: '))
    ingresso.venda_ingresso(tipo, sessao)
    print('Venda realizada com sucesso')


def menu_listar_ingressos_vendidos():
    print ('\n Listar ingressos pelo código da sessão \n')
    cod = int(input('Código da sessão:'))
    lista = ingresso.listar_ingressos_vendidos(cod)
    if (lista==None):
        print ('Não foram vendidos ingressos para esta sessão')
    else:
        for l in lista:
            imprimir_ingresso(l)


def menu_listar_todos():
    print ('\n Listar Ingressos \n')
    ingressos = ingresso.listar_ingressos()
    for i in ingressos:
        if (i==None):
            print('Nenhum Ingresso Disponível')
        else:
            imprimir_ingresso(i)


def menu_buscar_ingresso():
    print ('\n Buscar Ingresso pelo código\n')
    cod = int(input('Código do ingresso:'))
    i = ingresso.buscar_ingresso(cod)
    if (i==None):
        print('Nenhum ingresso encontrado')
    else:
        imprimir_ingresso(i)


def menu_remover():
    print ('\n Remover Ingresso \n')
    cod = int(input('Código do ingresso: '))
    i = ingresso.remover_ingresso(cod)
    if(i==False):
        print('Ingresso não encontrado')
    else:
        print('Ingresso removido')

def menu_excluir_todos():
    i = ingresso.remover_todos_ingressos()
    if(i==True):
        print('Ingressos Removidos')


def mostrar_menu():
    run_ingresso=True
    menu = ("\n ----------------- \n"+
                    "(1) Venda de Ingressos \n"+
                    "(2) Listar Ingressos por Código de Sessão \n"+
                    "(3) Listar Ingressos disponíveis \n"+
                    "(4) Buscar Ingresso por Código \n"+
                    "(5) Remover Ingresso \n"+
                    "(6) Remover todos os Ingressos \n"+
                    "(0) Voltar \n"+
                    " --------------- \n")

    while (run_ingresso):
        print (menu)
        op=int(input("Digite a sua escolha: "))

        if(op==1):
            menu_venda()
        elif(op==2):
            menu_listar_ingressos_vendidos()
        elif(op==3):
            menu_listar_todos()
        elif(op==4):
            menu_buscar_ingresso()
        elif(op==5):
            menu_remover()
        elif(op==6):
            menu_excluir_todos()
        elif(op==0):
            run_ingresso=False

if __name__=="__main__":
    mostrar_menu()
