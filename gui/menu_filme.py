from logica import filme

def imprimir_filme(filme):
    print("Código: ",filme[0])
    print('Titulo: ',filme[1])
    print('Duração: ',filme[2])
    print('Classficação: ',filme[3])
    print('Diretor: ',filme[4])
    print('Distribuidora: ',filme[5])
    print('Status: ',filme[6])
    print('Gênero: ',filme[7])
    print('\n')

def menu_cadastrar():
    print("\n Cadastrar filme \n")
    titulo=str(input('Titulo: '))
    duracao=str(input('Duração: '))
    classificacao=str(input('Classificação: '))
    diretor=str(input('Diretor: '))
    distribuidora=str(input('Distribuidora: '))
    status=str(input('Status: '))
    genero=str(input('Gênero: '))
    filme.cadastrar_filme(titulo,duracao,classificacao,diretor,distribuidora,status,genero)
    print('CADASTRO REALIZADO COM SUCESSO')

def buscar_filme():
    print('\n Buscar filme por código \n')
    cod=int(input('Código: '))
    f=filme.buscar_filme(cod)
    if(f==None):
        print('filme não encontrado')
    else:
        imprimir_filme(f)

def menu_remover():
    print("\n Remover filme \n")
    cod=int(input("Código: "))
    f=filme.remover_filme(cod)
    if(f==False):
        print("filme não encontrado")
    else:
        print("filme removido")

def menu_listar():
    print("\n Listar filmes \n")
    filmes=filme.buscar_filmes()
    for f in filmes:
        if (f==None):
            print('NENHUM FILME ENCONTRADO')
        else:
            imprimir_filme(f)
    

def menu_excluir_todos():
    f=filme.remover_todos_filmes()
    if(f==True):
        print('filmes removidos')


def mostrar_menu():
    run_filme=True
    menu=("\n----------------\n"+
             "(1) Adicionar novo filme \n" +
             "(2) Listar filmes \n" +
             "(3) Buscar filme por código \n" +
             "(4) Remover filme \n" +
             "(5) Remover todos os filmes \n" +
             "(0) Voltar\n"+
            "----------------")

    while(run_filme):
        print (menu)
        op = int(input("Digite sua escolha: "))

        if (op == 1):
            menu_cadastrar()
        elif(op == 2):
            menu_listar()
        elif(op == 3):
            buscar_filme()
        elif (op == 4):
            menu_remover()
        elif (op == 5):
            menu_excluir_todos()
        elif (op == 0):
            run_filme = False

if __name__ == "__main__":
    mostrar_menu()
