from logica import sessao
from logica import filme
from logica import sala
from gui import menu_filme
from gui import menu_sala

def imprimir_sessao(sessao):
    codigo = sessao[0]
    filme = sessao[1]
    sala = sessao[2]
    horario = sessao[3]
    print ('Código sessão:', codigo)
    print ('Filme:', filme)
    print ('Sala:', sala)
    print ('Horario', horario)
    print ('\n')

def menu_cadastrar():
    print ('\n Cadastrar Sessão \n')
    filme = int(input('Digite o código do filme: '))
    sala = int(input('Digite o codigo da sala: '))
    horario = str(input('Digite o horario: '))
    sessao.cadastrar_sessao(filme,sala,horario)
    print('CADASTRO REALIZADO COM SUCESSO')

def menu_buscar_sessao():
    print ('\n Buscar sessão pelo código \n')
    cod= int(input('Código da sessão:'))
    s=sessao.recuperar_sessao(cod)
    if (s==None):
        print('Sessão não encontrada')
    else:
        imprimir_sessao(s)

def menu_listar():
    print ('\n Listar sessões \n')
    sessoes = sessao.lista_sessao()
    for s in sessoes:
        if (s==None):
            print('NENHUMA SESSÃO ENCONTRADA')
        else:
            imprimir_sessao(s)


def menu_remover():
    print ('\n Remover sessão  \n')
    cod = int(input('Código: '))
    s=sessao.remover_sessao(cod)
    if(s==False):
        print('Sessão não encontrada')
    else:
        print('Sessão removida')

def menu_excluir_todas():
    s=sessao.remover_todas_sessoes()
    if(s==True):
        print('Sessões removidas')

def verificar_lotacao():
    print ('\n Verificar lotação por código \n')
    cod = int(input('Código: '))
    lot=sessao.verificar_lotacao(cod)
    print("Lotação de no maximo: ",lot, "pessoas")


def mostrar_menu():
    run_sessao=True
    menu = ("\n ----------------- \n"+
                    "(1) Adicionar nova sessão \n"+
                    "(2) Listar Sessões \n"+
                    "(3) Buscar sessão pelo código \n"+
                    "(4) Remover sessão \n"+
                    "(5) Remover todas sessões \n"+
                    "(6) Verificar lotação da sessão \n"+
                    "(0) Voltar \n"+
                    " --------------- \n")

    while (run_sessao):
            print (menu)
            op=int(input("Digite a sua escolha: "))

            if(op==1):
                menu_cadastrar()
            elif(op==2):
                menu_listar()
            elif(op==3):
                menu_buscar_sessao()
            elif(op==4):
                menu_remover()
            elif(op==5):
                menu_excluir_todas()
            elif(op==6):
                verificar_lotacao()
            elif(op==0):
                run_sessao=False

if __name__=="__main__":
    mostrar_menu()
