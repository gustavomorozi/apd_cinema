from logica import elenco
from logica import filme
from logica import ator
from gui import menu_filme
from gui import menu_ator

def imprimir_elenco(elenco):
    codigo = str(elenco[0])
    f = str(elenco[1])
    n = str(elenco[2])
    t = str(elenco [3])
    print('Codigo',codigo)
    print('Filme:',f)
    print('Nome:',n)
    print('Tipo:',t)
    print('\n')

def menu_cadastrar():
    print ("\n Cadastrar Elenco \n")
    filme = int(input('Código do filme: ' ))
    nome = int(input('Codigo do ator:  '))
    tipo= str(input('Digite o tipo:'))
    elenco.cadastrar_elenco(filme,nome,tipo)
    print('CADASTRO REALIZADO COM SUCESSO')

def menu_buscar_elenco():
    print ('\n Buscar elenco por código\n')
    cod= int(input("Codigo: "))
    e=elenco.buscar_elenco_codigo(cod)
    if (e==None):
        print ('Elenco não encontrado')
    else:
        imprimir_elenco(e)

def menu_buscar_elenco_filme():
    print ('\n Buscar elenco por filme\n')
    cod= int(input("Codigo do filme: "))
    elenc=elenco.buscar_elenco_filme(cod)
    if (elenc==None):
        print ('Elenco não encontrado')
    else:
        for e in elenc:
            imprimir_elenco(e)


def menu_listar():
    print('\n Listar elencos \n')
    elencos=elenco.lista_elencos()
    for e in elencos:
        if (e==None):
            print('Nenhum elenco encontrado')
        else:
            imprimir_elenco(e)

def menu_remover():
    print ("\n Remover por código do elenco \n")
    cod=int(input("Código: "))
    e=elenco.remover_elencos(cod)
    if(e==False):
        print("Elenco não encontrado")
    else:
        print ("Elenco removido")

def menu_excluir_todos():
    e=elenco.remover_todos_elencos()
    if (e==True):
        print("Elencos removidos")

def mostrar_menu():
    run_elenco=True
    menu=("\n-------------------\n"+
             "(1) Adicionar novo elenco \n"+
             "(2) Listar Elencos \n"+
             "(3) Buscar elenco por código \n"+
             "(4) Remover elenco\n"+
             "(5) Remover todos elencos\n"+
             "(6) Buscar elenco por filme\n"+
             "(0) Voltar \n"+
             "-----------------\n")
    while(run_elenco):
        print(menu)
        op=int(input("Digite sua escolha: "))

        if(op==1):
            menu_cadastrar()
        elif(op==2):
            menu_listar()
        elif(op==3):
            menu_buscar_elenco()
        elif(op==4):
            menu_remover()
        elif(op==5):
            menu_excluir_todos()
        elif(op==6):
            menu_buscar_elenco_filme()
        elif(op==0):
            run_elenco= False

if __name__=="__main__":
    mostrar_menu()
