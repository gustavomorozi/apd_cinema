from logica import sala

def imprimir_sala(sala):
    print("Código: ",sala[0])
    print('Lotação: ',sala[1])
    print('Status: ',sala[2])
    print('\n')

def menu_cadastrar():
    print("\n Cadastrar sala \n")
    lotacao=str(input('Lotação: '))
    sala.cadastrar_sala(lotacao)

def buscar_sala():
    print('\n Buscar sala por código \n')
    cod=int(input('Código : '))
    s=sala.buscar_sala(cod)
    if(s==None):
        print('sala não encontrada')
    else:
        imprimir_sala(s)

def menu_remover():
    print("\n Remover sala \n")
    cod=int(input("Código: "))
    s=sala.remover_sala(cod)
    if(s==False):
        print("sala não encontrada")
    else:
        print("sala removida")

def menu_listar():
    print("\n Listar salas \n")
    salas=sala.buscar_salas()
    for s in salas:
        if (s==None):
            print('Nenhuma sala encontrada')
        else:
            imprimir_sala(s)


def menu_excluir_todos():
    s=sala.remover_todos_salas()
    if(True):
        print('salas removidas')

def menu_status_livre():
    print('\n Definir status livre \n')
    cod=int(input('Código :'))
    s=sala.definir_status_livre(cod)
    if(s==True):
        print('Alteração feita com sucesso')
    else:
        print('Não encontrado')

def menu_status_ocupada():
    print('\n Definir status livre \n')
    cod=int(input('Código : '))
    s=sala.definir_status_ocupada(cod)
    if(s==True):
        print('Alteração feita com sucesso')
    else:
        print('Não encontrado')

def mostrar_menu():
    run_sala=True
    menu=("\n----------------\n"+
             "(1) Adicionar nova sala \n" +
             "(2) Listar salas \n" +
             "(3) Buscar sala por código \n" +
             "(4) Remover sala \n" +
             "(5) Remover todas salas \n" +
             "(6) Definir sala ocupada \n" +
             "(7) Definir sala livre \n" +
             "(0) Voltar\n"+
            "----------------")

    while(run_sala):
        print (menu)
        op = int(input("Digite sua escolha: "))

        if (op == 1):
            menu_cadastrar()
        elif(op == 2):
            menu_listar()
        elif(op == 3):
            buscar_sala()
        elif (op == 4):
            menu_remover()
        elif (op == 5):
            menu_excluir_todos()
        elif (op == 6):
            menu_status_ocupada()
        elif (op == 7):
            menu_status_livre()
        elif (op == 0):
            run_sala = False

if __name__ == "__main__":
    mostrar_menu()
