from logica import ator

def imprimir_ator(ator):
    print("Código: ",ator[0])
    print('Nome: ',ator[1])
    print('Nacionalidade: ',ator[2])
    print('Idade: ',ator[3])
    print('\n')

def menu_cadastrar():
    print("\n Cadastrar Ator \n")
    nome=str(input('Nome: '))
    nacionalidade=str(input('Nacionalidade: '))
    idade=int(input('Idade: '))
    ator.cadastrar_ator(nome,nacionalidade,idade)
    print('CADASTRO REALIZADO COM SUCESSO')

def buscar_ator():
    print('\n Buscar ator por código \n')
    cod=int(input('Código: '))
    a=ator.buscar_ator(cod)
    if(a==None):
        print('Ator não encontrado')
    else:
        imprimir_ator(a)

def menu_remover():
    print("\n Remover ator \n")
    cod=int(input("Código: "))
    a=ator.remover_ator(cod)
    if(a==False):
        print("Ator não encontrado")
    else:
        print("Ator removido")

def menu_listar():
    print("\n Listar atores \n")
    atores=ator.buscar_atores()
    for a in atores:
        if (a==None):
            print('NENHUM ATOR ENCONTRADO')
        else:
            imprimir_ator(a)


def menu_excluir_todos():
    a=ator.remover_todos_atores()
    if(a==True):
        print('Atores removidos')


def mostrar_menu():
    run_ator=True
    menu=("\n----------------\n"+
             "(1) Adicionar novo ator \n" +
             "(2) Listar atores \n" +
             "(3) Buscar ator por código \n" +
             "(4) Remover ator \n" +
             "(5) Remover todos atores \n" +
             "(0) Voltar\n"+
            "----------------")

    while(run_ator):
        print (menu)
        op = int(input("Digite sua escolha: "))

        if (op == 1):
            menu_cadastrar()
        elif(op == 2):
            menu_listar()
        elif(op == 3):
            buscar_ator()
        elif (op == 4):
            menu_remover()
        elif (op == 5):
            menu_excluir_todos()
        elif (op == 0):
            run_ator = False

if __name__ == "__main__":
    mostrar_menu()
