from logica import ator
from gui import menu_ator

from logica import filme
from gui import menu_filme

from logica import sala
from gui import menu_sala

from logica import elenco
from gui import menu_elenco

from logica import sessao
from gui import menu_sessao

from logica import ingresso
from gui import menu_ingresso

def inicializar_dados():
    ator.iniciar_atores()
    filme.iniciar_filmes()
    sala.iniciar_salas()
    elenco.iniciar_elencos()
    sessao.iniciar_sessoes()
    ingresso.iniciar_ingressos()



def mostrar_menu():
    run_menu = True

    inicializar_dados()


    menu = ("\n----------------\n"+
             "(1) Menu Ator \n" +
             "(2) Menu Filme \n" +
             "(3) Menu Sala \n" +
             "(4) Menu Elenco \n"+
             "(5) Menu Sessão \n"+
             "(6) Menu Ingresso \n"+
             "(0) Sair\n"+
            "----------------")

    while(run_menu):
        print (menu)

        op = int(input("Digite sua escolha: "))

        if (op == 1):
            menu_ator.mostrar_menu()
        if (op == 2):
            menu_filme.mostrar_menu()
        if (op == 3):
            menu_sala.mostrar_menu()
        if (op == 4):
            menu_elenco.mostrar_menu()
        if (op == 5):
            menu_sessao.mostrar_menu()
        if (op == 6):
            menu_ingresso.mostrar_menu()
        elif (op == 0):
            print ("Saindo do programa...")
            run_menu = False
        else:
            print ("Valor invalido")
