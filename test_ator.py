import unittest
from logica import ator

class TesteAtor(unittest.TestCase):

        def setUp(self):
            ator.cadastrar_ator('Bradd Pitt', 'Americano',21)
            ator.cadastrar_ator('Adriane Galisteu', 'Brasileira',29)
            ator.remover_todos_atores()


        def test_cadastrar_ator(self):
            ator.cadastrar_ator('Bradd Pitt', 'Americano',21)
            ator.cadastrar_ator('Adriane Galisteu', 'Brasileira',29)
            self.assertEqual(2,len(ator.atores))

        def test_buscar_ator(self):
            ator.cadastrar_ator('Bradd Pitt', 'Americano',21)
            self.assertIsNone(ator.buscar_ator(0))

        def test_remover_ator(self):
            ator.cadastrar_ator('Bradd Pitt', 'Americano',21)
            ator.cadastrar_ator('Adriane Galisteu', 'Brasileira',29)
            ator.remover_ator(0)
            a= ator.buscar_ator(0)

            self.assertIsNone(a)

        def test_remover_todos_atores(self):
            ator.cadastrar_ator('Bradd Pitt', 'Americano',21)
            ator.cadastrar_ator('Adriane Galisteu', 'Brasileira',29)
            ator.remover_todos_atores()
            a = ator.buscar_atores()
            self.assertEqual([], a)


        def test_iniciar_atores(self):
            ator.iniciar_atores()
            atores = ator.buscar_atores()
            self.assertEqual(2,len(atores))

        def test_sem_ator(self):
            atores=ator.buscar_atores()
            self.assertEqual(0,len(atores))


if __name__=='__main__':
    unittest.main()
