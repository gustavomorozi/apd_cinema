import unittest
from logica import ingresso


class Testeingresso(unittest.TestCase):

        def setUp(self):
            ingresso.venda_ingresso('Meia', 1)
            ingresso.venda_ingresso('Inteira', 2)
            ingresso.remover_todos_ingressos()


        def test_venda_ingresso(self):
            ingresso.venda_ingresso('Meia', 1)
            ingresso.venda_ingresso('Inteira', 2)
            self.assertEqual(2,len(ingresso.ingressos))

        def test_buscar_ingresso(self):
            ingresso.venda_ingresso('Meia', 1)
            self.assertIsNone(ingresso.buscar_ingresso(0))

        def test_remover_ingresso(self):
            ingresso.venda_ingresso('Meia', 1)
            ingresso.venda_ingresso('Inteira', 2)
            ingresso.remover_ingresso(0)
            i= ingresso.buscar_ingresso(0)

            self.assertIsNone(i)

        def test_remover_todos_ingressos(self):
            ingresso.venda_ingresso('Meia', 1)
            ingresso.venda_ingresso('Inteira', 2)
            ingresso.remover_todos_ingressos()
            i = ingresso.listar_ingressos()
            self.assertEqual([], i)


        def test_iniciar_ingressos(self):
            ingresso.iniciar_ingressos()
            ingressos = ingresso.listar_ingressos()
            self.assertEqual(2,len(ingressos))

        def test_sem_ingresso(self):
            ingressos=ingresso.listar_ingressos()
            self.assertEqual(0,len(ingressos))


if __name__=='__main__':
    unittest.main()
