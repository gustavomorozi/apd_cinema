import unittest
from logica import sala

class Testesala(unittest.TestCase):

        def setUp(self):
            sala.cadastrar_sala(30)
            sala.cadastrar_sala(40)
            sala.remover_todos_salas()


        def test_cadastrar_sala(self):
            sala.cadastrar_sala(30)
            sala.cadastrar_sala(40)
            self.assertEqual(2,len(sala.salas))

        def test_buscar_sala(self):
            sala.cadastrar_sala(30)
            self.assertIsNone(sala.buscar_sala(0))

        def test_remover_sala(self):
            sala.cadastrar_sala(30)
            sala.cadastrar_sala(40)
            sala.remover_sala(0)
            a= sala.buscar_sala(0)

            self.assertIsNone(a)

        def test_remover_todos_salas(self):
            sala.cadastrar_sala(30)
            sala.cadastrar_sala(40)
            sala.remover_todos_salas()
            a = sala.buscar_salas()
            self.assertEqual([], a)


        def test_iniciar_salas(self):
            sala.iniciar_salas()
            salas = sala.buscar_salas()
            self.assertEqual(2,len(salas))

        def test_sem_sala(self):
            salas=sala.buscar_salas()
            self.assertEqual(0,len(salas))


if __name__=='__main__':
    unittest.main()
